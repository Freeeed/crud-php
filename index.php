<?php
$db_host = "localhost";
$db_name = "bookmark";
$db_user = "root";
$db_pass = "";

$bdd = new PDO("mysql:host=" . $db_host . ";dbname=" . $db_name, $db_user, $db_pass);

$valid = true;
$errors = array();
// traitement du formulaire
if (isset($_POST['nom']) && isset($_POST['url'])) {
    if (!isset($_POST['check'])){
        $valid = false;
        $errors['check'] = "<div class='error'>Cochez au moin une categorie.<img class='smil' src='img/devil.png'></div>";
    }
    if (empty($_POST['nom'])) {
        $valid = false;
        $errors['nom'] = "<div class='error'>Champ Requis.<img class='smil' src='img/devil.png'></div>";
    }
    if (empty($_POST['url'])) {
        $valid = false;
        $errors['url'] = "<div class='error'>Champ Requis.<img class='smil' src='img/devil.png'></div>";
    }
    if ($valid === true) {
        $sql = 'INSERT INTO link SET nom=:nom, url=:url';
        $req = $bdd->prepare($sql);
        $req->execute(array('nom' => $_POST['nom'], 'url' => $_POST['url']));
        $id_link = $bdd->lastInsertId();
        $sql = 'INSERT INTO link_categorie (id_link, id_categorie) VALUES (:id_link, :id_categorie)';
        $req = $bdd->prepare($sql);
        foreach ($_POST['check'] as $id_cat) {
            $req->execute(array('id_categorie' => $id_cat, 'id_link' => $id_link));
        }
    }
} 


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>CRUD</title>
</head>

<body>
    <div class="bod">
        <nav>
            <a href="/crudphp">
                <li>Accueil</li>
            </a>
        </nav>
    </div>
    <div class="tabadd">
        <table cellspacing="0">
            <thead>
                <td>ID</td>
                <td>NOM</td>
                <td>URL</td>
                <td>CATEGORIE</td>
            </thead>
            <?php
            $sql = "SELECT l.id, l.nom, l.url, GROUP_CONCAT(c.nom SEPARATOR ' / ') as catname FROM link as l LEFT JOIN link_categorie as lc ON l.id = lc.id_link LEFT JOIN categorie as c ON c.id = lc.id_categorie GROUP BY l.id";
            $query = $bdd->prepare($sql);
            $query->execute();
            $lignes = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($lignes as $key => $ligne) {
                echo "<tr>
                <td>" . $ligne['id'] . "</td>
                <td>" . $ligne['nom'] . "</td>
                <td><a href='" . $ligne['url'] . "'target='_blank'>" . $ligne['url'] . "</a></td>
                <td>" . $ligne['catname'] . "</td>
                <td> <a href='modif.php?edit=" . $ligne['id'] . "'>Modifier</a></td>
                <td> <form action='connect.php' method='post'>
                    <input type=\"hidden\" name='delete' value=" . $ligne['id'] . " />
                    <button class=\"suppr\">Supprimer</button>
                </form>
                </td>
                </tr>";
            }

            ?>
        </table>


        <form class="addnew" action="index.php" method="post">
            <h2 class="titre">Pour ajouter un nouveau favoris :</h2>
            <h3>Choisir une ou plusieurs categorie</h3>
            <div class="cat">
                <?php
                $query = $bdd->prepare("SELECT * FROM categorie");
                $query->execute();
                $lignes = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($lignes as $key => $ligne) {
                    echo "
                <input type='checkbox' name='check[]' id='" . $ligne['id'] . "' value='" . $ligne['id'] . "'>
                <label class=\"yuyu\" for='" . $ligne['id'] . "'>" . $ligne['nom'] . "</label>";
                }
                
                if (isset($errors['check'])) {
                    echo $errors['check'];
                }
                ?>


            </div>
            <div class="name">
                <label>NOM :</label>
                <input class="champ" autocomplete="off" type="text" name="nom" placeholder="Nom du Nouveau Favoris">
                <?php
                if (isset($errors['nom'])) {
                    echo $errors['nom'];
                }
                ?>
            </div>

            <div class="lien">
                <label>URL :</label>
                <input class="champ" autocomplete="off" type="text" name="url" placeholder="Lien du Nouveau Favoris">
                <?php
                if (isset($errors['url'])) {
                    echo $errors['url'];
                }
                ?>
            </div>
            <input class="btn" type="submit" name="name" value="Ajouter">
        </form>
    </div>
</body>

</html>