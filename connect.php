<?php
$db_host = "localhost";
$db_name = "bookmark";
$db_user = "root";
$db_pass = "";

$bdd = new PDO("mysql:host=" . $db_host . ";dbname=" . $db_name, $db_user, $db_pass);

//permet de delete un data
if(isset($_POST['delete'])){
    $dlt='DELETE FROM link WHERE id=:delete';
    $reqe = $bdd->prepare($dlt);
    $reqe->execute(['delete'=>$_POST['delete']]);
}

//permet d'update data
if(isset($_POST['nouveau_nom'])&& isset($_POST['nouveau_url'])&& isset($_POST['id'])&& isset($_POST['id_categories'])){
    $upd='UPDATE link SET nom=:nom, url=:url WHERE id=:id';
    $reqet = $bdd->prepare($upd);
    $reqet->execute(array('id'=> $_POST['id'], 'nom'=> $_POST['nouveau_nom'], 'url'=> $_POST['nouveau_url']));
    $test='DELETE FROM link_categorie WHERE id_link=:id_link';
    $testi = $bdd->prepare($test);
    $testi->execute(['id_link'=>$_POST['id']]);
    $insert='INSERT INTO link_categorie (id_link, id_categorie) VALUES (:id_link, :id_categorie)';
    $inserting = $bdd->prepare($insert);
    foreach($_POST['id_categories'] as $id_cat){
        $inserting->execute(array('id_categorie'=>$id_cat, 'id_link'=>$_POST['id']));
    }
}

header('Location: http://localhost/crudphp/');