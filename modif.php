<?php
$db_host = "localhost";
$db_name = "bookmark";
$db_user = "root";
$db_pass = "";

$bdd = new PDO("mysql:host=" . $db_host . ";dbname=" . $db_name, $db_user, $db_pass);

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>CRUD</title>
</head>

<body>
    <div class="bod">
        <nav>
            <a href="/crudphp">
                <li>
                    <- Retour a l'Accueil</li>
            </a>
        </nav>
        <h2 class="titre">Apporter des changements a votre favoris ici :</h2>
        <form action='connect.php' method='post'>
            <div class="addnew">
                <?php
                echo "<div>";
                $query = $bdd->prepare("SELECT * FROM categorie");
                $query->execute();
                $checking = "SELECT c.id FROM link as l INNER JOIN link_categorie as lc ON l.id = lc.id_link INNER JOIN categorie as c ON c.id = lc.id_categorie WHERE l.id=:id ORDER BY c.id";
                $categorie_check = $bdd->prepare($checking);
                $categorie_check->execute(array('id'=>$_GET['edit']));
                $rows = $categorie_check->fetchAll(PDO::FETCH_BOTH);
                $lignes = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($lignes as $key=>$ligne) {
                    echo "
                    <input type='checkbox' name='id_categories[]' id='".$ligne['id']."' value='".$ligne['id'] ."' ";
                    foreach($rows as $row){
                        if($ligne['id']== $row[0]){
                            echo "checked";
                        }
                    }
                    echo ">
                    <label class=\"yuyu\" for='".$ligne['id']."'>" . $ligne['nom'] . "</label>";
                }
                echo "</div>";
                $query = $bdd->prepare("SELECT * FROM link WHERE id=" . $_GET['edit']);
                $query->execute();
                $ligne = $query->fetch(PDO::FETCH_BOTH);
                echo "
                    
                    <div class=\"namemod\"><label>NOUVEAU NOM :</label>
                    <input class=\"champ\" type='text' autocomplete='off' name='nouveau_nom' value=" . $ligne['nom'] . " /></div>
                    <div class=\"lienmod\"><label>NOUVEAU URL :</label>
                    <input class=\"champ\" type='text' autocomplete='off' name='nouveau_url' value=" . $ligne['url'] . " /></div>
                    <input type='hidden' name='id' value=" . $ligne['id'] . " />
                    <button class=\"btn\">Valider</button>";

                ?>
            </div>
        </form>
    </div>
</body>

</html>